KtoBLZCheck is a package providing up to date bankdata information
in several formats and a library to access and check account numbers
and bank codes of German and other banks.

Naming explanation: This library provides a check for "kto" (German
abbreviation for account number) and "blz" (German abbreviation for
bank identification code, A.B.A. code or similar).


Introduction
------------

Both a library for other programs as well as a short command-line tool
is available. It is possible to check pairs of account numbers and
bank codes (BLZ) of German and other banks, and to map bank codes (BLZ) to the
clear-text name and location of the bank. Python bindings are provided
as well (requires the python module "ctypes").


Requirements
============

* cmake
* c++ compiler (gcc, clang, mingw or msvc)
* python 2 interpreter (e.g package python-base on openSUSE)
* python 2 development package (e.g package python-devel on openSUSE)
* internet access for downloading bankdata files (optional)


Compiling Ktoblzcheck
=====================

First you need to unpack the source either by fetching from git with

    cd <some-root-dir>
    git clone https://git.code.sf.net/p/ktoblzcheck/code ktoblzcheck

This will create a directory named ktoblzcheck, which you need to specify
later as source directory.

or from a tarball located at https://sourceforge.net/projects/ktoblzcheck/files/

    cd <some-root-dir>
    tar -xzf <download-root>/ktoblzcheck-1.49.tar.gz

This will create a directory named ktoblzcheck-1.49, which you need to specify
later as source directory.

There are different ways to compile ktoblzcheck depending on the platform and/or
used compiler.


Unix like OS
------------

For compiling ktoblzcheck on Unix like OS, after you got the sources as described
above, you need to run:

    mkdir ktoblzcheck-build
    cd ktoblzcheck-build
    cmake <ktoblzcheck-src-dir> [<additional-cmake-options>] [1]
    make
    make install

[1] for available cmake options see below


GCC/Windows (MinGW32)
---------------------

You need to download a MingW compiler from http://sourceforge.net/projects/mingw-w64/.
After unpacking and placing the bin dir into the PATH environment variables you need to run

    mkdir ktoblzcheck-build
    cd ktoblzcheck-build
    cmake -G "MinGW Makefiles" <ktoblzcheck-src-dir> [<additional-cmake-options>] [1]
    mingw32-make
    mingw32-make install


MSVC/Windows
------------

For compiling ktoblzcheck for MSVC you need to open a MSVC command shell

    mkdir ktoblzcheck-build
    cd ktoblzcheck-build
    cmake -G "NMake Makefiles" <ktoblzcheck-src-dir> [<additional-cmake-options>] [1]
    nmake
    nmake install


Running
-------

  ktoblzcheck <your-bank-id> <your-account-id>

Some example BLZ;Account-ids are the following from Bundesbank:
64192220;12345008
20210200;0240334000
56061472;0000191919

The ktoblzcheck command has several optional arguments; use "--help"
to see a summary. (The argument --outformat by default is
"multiline". The other choice, "--outformat=oneline", gives you one
line of result with the following tab-delimited fields: "bank name,
bank city, result of check, BLZ, accountId, name of the checking
algorithm". This format is more suitable if multiple account numbers
are read from an input file using the option --infile.)

Running tests
-------------

ktoblzcheck contains several test applications to check the implementation and
some parts of the build system.

These tests can be run after building with the following command:

    cd ktoblzcheck-build
    <used-make-tools> test


Additional CMake Options
------------------------

The cmake build system provides some additional options, which could be specified on
the cmake command line in the form -D<option-name>=<value>

    option name                    platform             value              default value
    -------------------------------------------------------------------------------------
    ENABLE_BANKDATA_DOWNLOAD         all               OFF/ON                  ON
    Enable download of raw bank data file - if this option is set to OFF, cmake uses
    already present bank data file
    
    INSTALL_RAW_BANKDATA_FILE        all               OFF/ON                  OFF
    Install raw bank data file - this option is useful for 3rdparty applications like kmymoney

    INSTALL_SEPA_BANKDATA_FILE       all               OFF/ON                  OFF
    Install sepa provider data file in csv format

    ISCC_DIR                        Windows              path                 not set
    Specify the installation directory of the inno setup compiler that is required
    to create a setup installation program.
    
    USE_WINE                        Windows              OFF/ON                OFF
    Enable support to run cross compiled applications on Linux with wine.

Using ktoblzcheck
=================

To use the library in your own application it is necessary to find an installed development
package from ktoblzcheck. The necessary procedure depends on the build system used:

cmake
-----

Add the following cmake fragment to the top level CMakeLists.txt file:

    find_package(KtoBlzCheck)
    ...
    if(KtoBlzCheck_FOUND)
        add_executable(myapp mysource.cpp)
        target_link_libraries(myapp ktoblzcheck)
    endif()


Autotools
---------

... to be finished


C++ Code
--------

You can then use the following code fragment in your C++ code, for example:

    #include <ktoblzcheck.h>
    ...
    AccountNumberCheck var;
    std::string bankId = "64192220";
    std::string accountId = "12345008";
    Result result = var.check(bankId, accountId);

For more information, see \ref AccountNumberCheck.

    #include <iban.h>
    ...
    IbanCheck var;
    std::string iabn = "DE07123412341234123412";
    Result result = var.check(iban);

For more information, see \ref IbanCheck.


Installer
---------

- On Windows you may install the "Inno Setup Compiler" from
  http://www.jrsoftware.org/isdl.php, run cmake with the install path added
  to the cmake command line e.g.

   cmake <ktoblzcheck-src-dir> -DISCC_DIR=<inno-setup-install-dir>

  and run

   <used-make-tools>
   <used-make-tools> setup

  so that a self-installing setup.exe file will be created.


Common CMake targets
--------------------

The cmake build system provides several build targets, which could be displyed by running

    <used-make-tool> help

in the build directory. Common targets are:

    dist   - build source tarball
    rpm    - build binary rpm
    srpm   - build source rpm
    test   - run test suit


Bank List Format
----------------

A file with all bank codes (BLZ Datei) can regularly be retrieved from
the Deutsche Bundesbank at bundesbank.de, more specifically from

http://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen

Updating to the most recent bank data file is done automatically by
the build system if cmake configure option -DENABLE_BANKDATA_DOWNLOAD=1
has been set (which is the default).

Since version 1.16, ktoblzcheck installs multiple bankdata files which
are valid at different dates. Each of the bank code files from the
Deutsche Bundesbank is valid for three (3) months.  Ktoblzcheck ships
with the bankdata file from several validity periods: One file is
valid at the date of release; and the second file is valid in the three
subsequent months (i.e. it is newer). If even the newest file is
outdated at the day of when libktoblzcheck is used, a warning line
will be printed to stderr and the newest file will be used anyway.

Note: The available files (marked by the first valid day as part of
the filename) and their validity period are looked up by simply taking
all files in the BANKDATA_PATH folder into consideration. Hence, you
can install a new bank data file by copying the correct file format
into that folder, which is done automatically by the cmake build system

Note 2nd: The bank data file "bankdata.txt" with no date in the file
name is not used anymore. It used to be a one-to-one copy of the
newest file, but with ktoblzcheck-1.35 this file is neither used nor
shipped anymore.

Note 3rd: All of the above is ignored if a bankdata file is explicitly
requested by the command line argument --file=... . In that case
the specified file is used and no further date comparisons are made.

The bankdata.txt file used by ktoblzcheck contains less information
than the file provided by the Bundesbank. Namely, it contains only
four tab-delimited columns and only those lines that refer to the main
institutes instead of all branches. The provided python script
src/bankdata/bundesbank.py (provided by Ralf Habacker) will automatically
convert the Bundesbank's ascii file into the required ktoblzcheck format.

If you got the Bundesbank BLZ file in excel format, the four columns
for bankdata.txt are A, O, G, J, in that order, and it has only those
lines which have a '1' in column B.


Authors
-------

The original author was Fabian Kaiser <fabian@openhbci.de>. The
current maintainer is Christian Stimming <stimming@tuhh.de> and
Alexander Kurz <sf2@blala.de>.

For contacting the authors, please write to the mailing list
< ktoblzcheck-cvs@lists.sourceforge.net >. This list is subscriber-only
which means you need to subscribe if you want to post to the
list. Subscription information can be found on
https://sourceforge.net/projects/ktoblzcheck/lists/ktoblzcheck-cvs


Related projects
----------------

A ruby interface to ktoblzcheck:
https://rubygems.org/gems/rbktoblzcheck/versions/0.1.2

A different implementation of this checking algorithms:
http://kontocheck.sourceforge.net/
