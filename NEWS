2020-01-20: Release ktoblzcheck-1.51

Add fetching of sepa providers and install them as csv file
Add supported short options to ktoblzcheck usage output
Fix finding files on windows
Fix bug not finding default bank data file when XDG_DATA_DIRS is not set (#2)

2019-09-19: Release ktoblzcheck-1.50

Add cmake option to install the raw bankdata file
Add german iban check
Add support for environment variable XDG_DATA_DIRS also on Windows required by test cases
Add support for running cross compiled applications on linux
Completed cmake support on Linux and Windows
Dropped autotools build system
Fix structure documentation in ibandata.txt
Fix test cases
Merge text from README*, INSTALL* into README.md
New bank data file added
Replaced perl scripts by python scripts to reduce the number of requirements
Update doxygen config file to version 1.8.14
Use README.md as main page for doxygen

2017-03-02: Release ktoblzcheck-1.49

2014-11-23: Release ktoblzcheck-1.48

New bank data file added. New method E1 implemented.

2014-09-23: Release ktoblzcheck-1.47

Fix crash due to XDG_DATA_DIRS lookup bug.

2014-08-12: Release ktoblzcheck-1.46

New bank data file added. Replace binreloc with check of
XDG_DATA_DIRS.

2014-02-16: Release ktoblzcheck-1.45

New bank data file added. Minor compiler warning fixed.

2013-12-30: Release ktoblzcheck-1.44

Modified method 95. New bank data file added.

2013-05-29: Release ktoblzcheck-1.43

Modified methods C6, D1, 51, 84, 57. New bank data file added.

2013-05-29: Release ktoblzcheck-1.42

New method E0. New bank data file added.

2013-03-02: Release ktoblzcheck-1.41

New bank data file added.

2012-09-05: Release ktoblzcheck-1.40

New bank data file added.

2012-05-13: Release ktoblzcheck-1.39

New bank data file and new method D9 added.

2012-03-21: Release ktoblzcheck-1.38

New bank data file added.

2011-12-04: Release ktoblzcheck-1.37

New bank data file added.

2011-09-26: Release ktoblzcheck-1.36

Fix crash on start up (failed assertion).

2011-09-06: Release ktoblzcheck-1.35

New modified methods B6 and D1. Bank data files will now be used as
soon as they are copied or downloaded into the installation directory
(e.g. by running online_update.pl) without newly compiling the
library.

2011-08-25: Release ktoblzcheck-1.34

New bank data file added.

2011-06-07: Release ktoblzcheck-1.33

Fixed erroneous library version number.

2011-05-21: Release ktoblzcheck-1.32

New bank data file added. Added new Method D7, D8, modified methods
B8, C6 and D4.

2011-02-11: Release ktoblzcheck-1.31

Fixing an error in the build system.

2011-02-10: Release ktoblzcheck-1.30

New bank data file added. Added new Method D6, changed Method D1.

2010-11-20: Release ktoblzcheck-1.29

New bank data file added. Added new method D5.

2010-08-11: Release ktoblzcheck-1.28

New bank data file added. Fixed method 96 and 63.

2010-05-12: Release ktoblzcheck-1.27

New bank data file added. Modified Methods C6 and D1, added method D4.
Valid from 7. June 2010.

2010-03-10: Release ktoblzcheck-1.26

Fix broken loading of new bank data file.

2010-02-02: Release ktoblzcheck-1.25

New bank data file added. Method 23 fixed.

November 17th, 2009: Release ktoblzcheck-1.24

New bank data file added.

August 2nd, 2009: Release ktoblzcheck-1.23

New bank data file added.

June 20, 2009: Released ktoblzcheck-1.22

New bank data file added.

February 09, 2009: Released ktoblzcheck-1.21

Method C6 modified to include March'09 update.  New bank data file
added.

November 14, 2008: Released ktoblzcheck-1.20

Two new calculation methods have been added.  New bank data file
added.

August 12, 2008: Released ktoblzcheck-1.19

Two new calculation methods have been added.  Online-update accepts
wget instead of lynx as well.  New bank data file added.

May 16, 2008: Released ktoblzcheck-1.18

Two new calculation methods have been added.  New bank data file
added.

March 1, 2008: Released ktoblzcheck-1.17

One serious crasher bug on FreeBSD and Mac OSX has been
fixed. Handling of the timezone has been fixed. The code had been
cleaned up for being compiled with gcc-4.3. New bank data file added.

November 7, 2007: Released ktoblzcheck-1.16

New method "C7" has been added. Code has been cleaned up for
portability so that it runs on Windows again. Bankdata files are now
by default chosen according to their validity dates, so that the
2007-12-03 file will be used only after that date and the 2007-09-03
file will be used before that.

September 3, 2007: Released ktoblzcheck-1.15

The new method "C5" has been added. One bug in method "74" has been
fixed, and method "87" has been cleaned up. The bank data file as of
September 2007 has been included. Also, as a new feature bank data
files are additionally available with their validity date so that a
bankdata file for a particular date can be loaded, if available.

July 8, 2007: Released ktoblzcheck-1.14

Some so far undiscovered bugs in the account number checking method 74
have been fixed. The bank data file as of June 2007 has been included.

February 17, 2007: Released ktoblzcheck-1.13

In this release, several so far undiscovered bugs in the account
number checking methods 02 and some more have been fixed. News methods
C3, C4, and 57 have been added. The path name lookup of the bankdata
file has been changed to use runtime lookup on windows always, and on
Unix if it is available. The bank data file for March 2007 has been
included.

November 23, 2006: Released ktoblzcheck-1.12

This regular release includes the bank data file valid from
December 2006 onwards. Also, the python wrappers have been updated
for ctypes>0.9.9 thanks to Michael Dietrich. And the installation
files have been improved thanks to David Reiser.


August 15, 2006: Released ktoblzcheck-1.11

This regular release includes the bank data file valid from
September 2006 onwards. Several methods have been improved and
fixed by Alexander Kurz, including methods C1, C2, A8, and some
cleanup in B4 and B5. The documentation has been improved.

February 13, 2006: Released ktoblzcheck-1.10

This regular release includes the latest bank data file, valid
from March 2006 onwards. Also, problems with gcc2.x includes have
been fixed, as well as potential stack allocation overruns when
reading the data file.

July 20, 2004: Released ktoblzcheck-1.0

Mit dem Versionssprung in diesem Release wird auch offiziell bekannt
gegeben, daß der Quellcode schon lange sehr stabil läuft. Abgesehen
von den aktualisierten Bankdaten gab es keine inhaltlichen
Änderungen. Neu ist allerdings trotzdem, daß auch ein project file für
den Microsoft Visual Studio C++ compiler mitgeliefert wird.

November 19, 2003: Released ktoblzcheck-0.5

Die allerletzten fehlenden Algorithmen für die Nummernprüfung sind nun
dank Daniel Gloeckner <daniel-gl@gmx.net> ebenfalls eingebaut. Ein
fehlerhafter (selten benutzter) Algorithmus wurde dank des
ausführlichen Feedbacks eines Benutzers korrigiert. Die Bankdate ist
aktualisiert worden.


August 11, 2003: Released ktoblzcheck-0.4

Dank der großartigen Arbeit von Jens Gecius <jens@gecius.de> sind nun
alle bekannten Nummernprüf-Algorithmen aus der Bundesbank-Datei
implementiert. Diese Version kann nun für alle deutschen Banken die
Kontonummern prüfen.
